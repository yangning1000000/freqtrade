aws ecr get-login --no-include-email --region us-east-1 | awk '{printf $6}' | sudo docker login -u AWS --password-stdin https://725911963085.dkr.ecr.us-east-1.amazonaws.com
sudo docker pull 725911963085.dkr.ecr.us-east-1.amazonaws.com/alpha:latest
echo "Kill all existing docker process"
sudo docker kill $(docker ps -q)
echo "Start running docker image"
sudo docker run -v `pwd`/config.json:/freqtrade/config.json -v `pwd`/user_data/:/freqtrade/user_data -v `pwd`/orderbook.json:/freqtrade/orderbook.json 725911963085.dkr.ecr.us-east-1.amazonaws.com/alpha:latest monitor --strategy Simple &