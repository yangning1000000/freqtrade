from .base_websocket import Websocket
import json
import decimal
import traceback
import logging
import datetime
class BitmexWebsocket(Websocket):
    MAX_TABLE_LEN = 200
    def __init__(self):
        super().__init__()
        self.logger.setLevel(logging.ERROR)
        self.url = "wss://www.bitmex.com/realtime"
        self.orderbook = {"bids": {}, "asks": {}, "source": "bitmex"}
        self.skew = {}
    
    def _send_command(self):
        self.ws.send(json.dumps({"op": "subscribe", "args": ["orderBookL2:XBTUSD", "quote:XBTUSD"]}))

    def get_instrument(self, symbol):
        instruments = self.data['instrument']
        matchingInstruments = [i for i in instruments if i['symbol'] == symbol]
        if len(matchingInstruments) == 0:
            raise Exception("Unable to find instrument or index with symbol: " + symbol)
        instrument = matchingInstruments[0]
        # Turn the 'tickSize' into 'tickLog' for use in rounding
        # http://stackoverflow.com/a/6190291/832202
        instrument['tickLog'] = decimal.Decimal(str(instrument['tickSize'])).as_tuple().exponent * -1
        return instrument

    def _on_message(self, message):
        '''Handler for parsing WS messages.'''
        message = json.loads(message)
        self.logger.debug(json.dumps(message))
        # print(message)
        table = message['table'] if 'table' in message else None
        action = message['action'] if 'action' in message else None
        try:
            if 'subscribe' in message:
                if message['success']:
                    self.logger.debug("Subscribed to %s." % message['subscribe'])
                else:
                    self.error("Unable to subscribe to %s. Error: \"%s\" Please check and restart." %
                               (message['request']['args'][0], message['error']))
            elif 'status' in message:
                if message['status'] == 400:
                    self.error(message['error'])
                if message['status'] == 401:
                    self.error("API Key incorrect, please check and restart.")
            elif action:

                if table not in self.data:
                    self.data[table] = []

                if table not in self.keys:
                    self.keys[table] = []

                # There are four possible actions from the WS:
                # 'partial' - full table image
                # 'insert'  - new row
                # 'update'  - update row
                # 'delete'  - delete row
                if action == 'partial':
                    self.logger.debug("%s: partial" % table)
                    self.data[table] += message['data']
                    # Keys are communicated on partials to let you know how to uniquely identify
                    # an item. We use it for updates.
                    self.keys[table] = message['keys']
                elif action == 'insert':
                    self.logger.debug('%s: inserting %s' % (table, message['data']))
                    self.data[table] += message['data']

                    # Limit the max length of the table to avoid excessive memory usage.
                    # Don't trim orders because we'll lose valuable state if we do.
                    if table not in ['order', 'orderBookL2'] and len(self.data[table]) > BitmexWebsocket.MAX_TABLE_LEN:
                        self.data[table] = self.data[table][(BitmexWebsocket.MAX_TABLE_LEN // 2):]

                elif action == 'update':
                    self.logger.debug('%s: updating %s' % (table, message['data']))
                    # Locate the item in the collection and update it.
                    for updateData in message['data']:
                        item = findItemByKeys(self.keys[table], self.data[table], updateData)
                        if not item:
                            continue  # No item found to update. Could happen before push

                        # Log executions
                        if table == 'order':
                            is_canceled = 'ordStatus' in updateData and updateData['ordStatus'] == 'Canceled'
                            if 'cumQty' in updateData and not is_canceled:
                                contExecuted = updateData['cumQty'] - item['cumQty']
                                if contExecuted > 0:
                                    instrument = self.get_instrument(item['symbol'])
                                    self.logger.info("Execution: %s %d Contracts of %s at %.*f" %
                                             (item['side'], contExecuted, item['symbol'],
                                              instrument['tickLog'], item['price']))

                        # Update this item.
                        item.update(updateData)

                        # Remove canceled / filled orders
                        if table == 'order' and item['leavesQty'] <= 0:
                            self.data[table].remove(item)

                elif action == 'delete':
                    self.logger.debug('%s: deleting %s' % (table, message['data']))
                    # Locate the item in the collection and remove it.
                    for deleteData in message['data']:
                        item = findItemByKeys(self.keys[table], self.data[table], deleteData)
                        self.data[table].remove(item)
                else:
                    raise Exception("Unknown action: %s" % action)
        except:
            self.logger.error(traceback.format_exc())

    def __process(self):
        curPrice = (self.data["quote"][-1]["askPrice"] + self.data["quote"][-1]["bidPrice"]) / 2
        self.orderbook["price"] = curPrice
        d = datetime.datetime.utcnow()
        epoch = datetime.datetime(1970,1,1)
        t = int((d - epoch).total_seconds())
        self.orderbook["time"] = t
        for i in ["bids", "asks"]:
            for j in self.orderbook[i].keys():
                cumulative = 0
                delta = int(curPrice * float(j))
                for k in self.data["orderBookL2"]:
                    if (i == "bids" and k["side"] == "Buy" and curPrice >= k["price"] >= curPrice - delta) or (i == "asks" and k["side"] == "Sell" and curPrice <= k["price"] <= curPrice + delta):
                        cumulative += k["size"]
                self.orderbook[i][j] = cumulative/curPrice
        self.__update_skew()

    def __update_skew(self):
        for i in self.skew.keys():
            self.skew[i] = self.orderbook["bids"][i]/self.orderbook["asks"][i]
    
    def run(self, precision):
        # d = datetime.datetime.utcnow()
        # epoch = datetime.datetime(1970,1,1)
        # t = (d - epoch).total_seconds()
        # self.orderbook['time'] = int(t)
        if self.ws:
            self.__process()
            return self.orderbook, self.skew
        for i in ["bids", "asks"]:
            for j in precision:
                self.orderbook[i][j] = 0
        for i in precision:
            self.skew[i] = 0
        self.connect(self.url)
        return {}, {}
    

def findItemByKeys(keys, table, matchData):
    for item in table:
        matched = True
        for key in keys:
            if item[key] != matchData[key]:
                matched = False
        if matched:
            return item