from .base_websocket import Websocket
import json
import decimal
import traceback
import logging
import datetime
import urllib3

class DeribitWebsocket(Websocket):
    MAX_TABLE_LEN = 200
    def __init__(self):
        super().__init__()
        self.logger.setLevel(logging.ERROR)
        self.url = "wss://www.deribit.com/ws/api/v2/"
        self.orderbook = {"bids": {}, "asks": {}, "source": "deribit"}
        self.skew = {}
        self.localBook = {"bids": {}, "asks": {}}
        self.curPrice = 1

    def _send_command(self):
        self.ws.send(json.dumps({"params": {"interval": 10}, "method": "public/set_heartbeat", "jsonrpc": "2.0", "id": 0}))
        self.ws.send(json.dumps({"method": "public/subscribe", "params": {"channels": ["book.BTC-PERPETUAL.100ms", "ticker.BTC-PERPETUAL.100ms"]}}))

    def _on_message(self, message):
        '''Handler for parsing WS messages.'''
        message = json.loads(message)
        self.logger.debug(json.dumps(message))
        if "method" in message and message["method"] == "heartbeat":
            self.ws.send(json.dumps({"method": "public/test"}))
        elif "params" in message:
            if message["params"]["channel"] == "book.BTC-PERPETUAL.100ms":
                for i in ["bids", "asks"]:
                    for j in message["params"]["data"][i]:
                        if j[0] == "new" or j[0] == "change":
                            self.localBook[i][j[1]] = j[2]
                        elif j[0] == "delete":
                            self.localBook[i][j[1]] = 0
            elif message["params"]["channel"] == "ticker.BTC-PERPETUAL.100ms":
                self.curPrice = message["params"]["data"]["last_price"]
            else:
                pass
                # print(message)
# 
    def __process(self):
        if self.curPrice > 0:
            self.orderbook["price"] = self.curPrice
            d = datetime.datetime.utcnow()
            epoch = datetime.datetime(1970,1,1)
            t = int((d - epoch).total_seconds())
            self.orderbook["time"] = t
            for i in ["bids", "asks"]:
                for j in self.orderbook[i]:
                    if i == "bids":
                        self.orderbook[i][j] =  sum(map(float, [self.localBook['bids'][i] for i in list(self.localBook['bids']) if float(i) > self.curPrice * (1 - float(j))]))/self.curPrice
                    else:
                        self.orderbook[i][j] =  sum(map(float, [self.localBook['asks'][i] for i in list(self.localBook['asks']) if float(i) < self.curPrice * (1 + float(j))]))/self.curPrice
            self.__update_skew()

    def __update_skew(self):
        for i in self.skew.keys():
            self.skew[i] = self.orderbook["bids"][i]/self.orderbook["asks"][i]
    
    def run(self, precision):
        # d = datetime.datetime.utcnow()
        # epoch = datetime.datetime(1970,1,1)
        # t = (d - epoch).total_seconds()
        # self.orderbook['time'] = int(t)
        if self.ws:
            self.__process()
            return self.orderbook, self.skew
        for i in ["bids", "asks"]:
            for j in precision:
                self.orderbook[i][j] = 0
        for i in precision:
            self.skew[i] = 0
        self.connect(self.url)
        return {}, {}
    

# def findItemByKeys(keys, table, matchData):
#     for item in table:
#         matched = True
#         for key in keys:
#             if item[key] != matchData[key]:
#                 matched = False
#         if matched:
#             return item