import sys
import websocket
import threading
import traceback
import ssl
from time import sleep
import json
import decimal
import logging
# from market_maker.settings import settings
# from market_maker.auth.APIKeyAuth import generate_expires, generate_signature
# from market_maker.utils.log import setup_custom_logger
# # from market_maker.utils.math import toNearest
# from future.standard_library import hooks


# Connects to BitMEX websocket for streaming realtime data.
# The Marketmaker still interacts with this as if it were a REST Endpoint, but now it can get
# much more realtime data without heavily polling the API.
#
# The Websocket offers a bunch of data as raw properties right on the object.
# On connect, it synchronously asks for a push of all this data then returns.
# Right after, the MM can start using its data. It will be updated in realtime, so the MM can
# poll as often as it wants.
class Websocket():

    # Don't grow a table larger than this amount. Helps cap memory usage.
    # MAX_TABLE_LEN = 200

    def __init__(self):
        self.logger = logging.getLogger('rootd')
        # self.logger.setLevel(logging.DEBUG)
        # ch = logging.StreamHandler()
        # create formatter
        # formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
        # add formatter to ch
        # ch.setFormatter(formatter)
        # self.logger.addHandler(ch)
        self.__reset()
        self.ws = None

    def __del__(self):
        self.exit()

    def connect(self, endpoint=""):
        '''Connect to the websocket and initialize data stores.'''

        self.logger.debug("Connecting WebSocket.")

        # We can subscribe right in the connection querystring, so let's build that.
        # Subscribe to all pertinent endpoints

        self.logger.info("Connecting to %s" % endpoint)
        self.__connect(endpoint)
        self.logger.info('Connected to WS. Waiting for data images, this may take a moment...')

    #
    # Lifecycle methods
    #
    def error(self, err):
        self._error = err
        self.logger.error(err)
        self.exit()

    def exit(self):
        self.exited = True
        self.ws.close()

    #
    # Private methods
    #

    def __connect(self, wsURL):
        '''Connect to the websocket in a thread.'''
        self.logger.debug("Starting thread")

        ssl_defaults = ssl.get_default_verify_paths()
        sslopt_ca_certs = {'ca_certs': ssl_defaults.cafile}
        self.ws = websocket.WebSocketApp(wsURL,
                                         on_message=self._on_message,
                                         on_close=self.__on_close,
                                         on_open=self.__on_open,
                                         on_error=self.__on_error,
                                         header = {"Origin": "https://data.bitcoinity.org"}
                                         )
        # self.ws.run_forever()
        self.wst = threading.Thread(target=lambda: self.ws.run_forever(sslopt=sslopt_ca_certs))
        self.wst.daemon = True
        self.wst.start()
        self.logger.info("Started thread")

        # Wait for connect before continuing
        conn_timeout = 5
        while (not self.ws.sock or not self.ws.sock.connected) and conn_timeout and not self._error:
            sleep(1)
            conn_timeout -= 1

        if not conn_timeout or self._error:
            self.logger.error("Couldn't connect to WS! Exiting.")
            self.exit()
            sys.exit(1)

    def _send_command(self):
        pass
        # self.ws.send(json.dumps({"topic": "webs:data_books_cc_rel_all", "event": "phx_join", "payload": {}, "ref": "1"}))
        
    def _on_message(self, message):
        pass
    #     '''Handler for parsing WS messages.'''
    #     print(message)
    #     message = json.loads(message)
    #     if message["event"] == "new_msg":
    #         self.__process(message)
    #     if message["event"] == "phx_reply":
    #         self.ws.send(json.dumps({"event": "phx_reply", "payload": {"response": {}, "status": "ok"}, "ref": str(int(message["ref"]) + 1), "topic": "phoenix"}))

    # def __process(self, message):
    #     curPrice = (message["payload"]["data"]["data_books_cc"]["asks"]["x"][0] + message["payload"]["data"]["data_books_cc"]["bids"]["x"][0]) / 2
    #     for i in ["bids", "asks"]:
    #         for j in self.orderbook[i].keys():
    #             cumulative = 0
    #             # delta = int(curPrice * float(j))
    #             for k in message["payload"]["data"]["data_books_cc"][i]["values"].values():
    #                 cumulative += float(k[min(int(float(j)*10000)-1, len(k)-1)])
    #             self.orderbook[i][j] = cumulative
    #     self.__update_skew()

    # def __update_skew(self):
    #     for i in self.skew.keys():
    #         self.skew[i] = self.orderbook["bids"][i]/self.orderbook["asks"][i]
    #     print(self.skew)
    #     print(self.orderbook)
    
    def __on_open(self):
        self.logger.debug("Websocket Opened.")
        self._send_command()

    def __on_close(self):
        print("closed")
        self.logger.debug('Websocket Closed')
        self.exit()

    def __on_error(self, ws, error):
        print(error)
        if not self.exited:
            self.error(error)

    def __reset(self):
        self.data = {}
        self.keys = {}
        self.exited = False
        self._error = None
    
    # def run(self):
        # if self.ws:
        #     return self.orderbook, self.skew
        # self.connect("wss://data.bitcoinity.org/webs_bridge/websocket?vsn=1.0.0")
        # return self.orderbook, self.skew

# def findItemByKeys(keys, table, matchData):
#     for item in table:
#         matched = True
#         for key in keys:
#             if item[key] != matchData[key]:
#                 matched = False
#         if matched:
#             return item

# if __name__ == "__main__":
#     # create console handler and set level to debug
#         logger = logging.getLogger()
#         logger.setLevel(logging.DEBUG)
#         ch = logging.StreamHandler()
#         # create formatter
#         formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
#         # add formatter to ch
#         ch.setFormatter(formatter)
#         logger.addHandler(ch)
#         ws = BitcoinityWebsocket()
#         ws.logger = logger
#         ws.connect("wss://data.bitcoinity.org/webs_bridge/websocket?vsn=1.0.0")

    # while(ws.ws.sock.connected):
    #     sleep(1)

