from .base_websocket import Websocket
import json
import datetime
import threading

class BitcoinityWebsocket(Websocket):
    def __init__(self):
        super().__init__()
        self.url = "wss://data.bitcoinity.org/webs_bridge/websocket?vsn=1.0.0"
        self.orderbook = {"bids": {}, "asks": {}, "source": "bitcoinity"}
        self.skew = {}
        self.ref = 0
    
    def _send_command(self):
        self.ws.send(json.dumps({"topic": "webs:data_books_cc_rel_all", "event": "phx_join", "payload": {}, "ref": "1"}))
        self.ref += 1

    def _send_heartbeat(self):
        self.ws.send(json.dumps({"topic": "phoenix", "event": "heartbeat", "payload": {}, "ref": self.ref}))
        self.ref += 1

    def _on_message(self, message):
        '''Handler for parsing WS messages.'''
        # print(message)
        message = json.loads(message)
        if message["topic"] == "webs:data_books_cc_rel_all":
            self.__process(message)
        # if message["event"] == "phx_reply":
        #     self.ws.send(json.dumps({"event": "phx_reply", "payload": {"response": {}, "status": "ok"}, "ref": str(int(message["ref"]) + 1), "topic": "phoenix"}))

    def __process(self, message):
        if "data" in message["payload"]:
            # curPrice = (message["payload"]["data"]["data_books_cc"]["asks"]["x"][0] + message["payload"]["data"]["data_books_cc"]["bids"]["x"][0]) / 2
            d = datetime.datetime.utcnow()
            epoch = datetime.datetime(1970,1,1)
            t = int((d - epoch).total_seconds())
            self.orderbook["time"] = t
            for i in ["bids", "asks"]:
                for j in self.orderbook[i].keys():
                    cumulative = 0
                    # delta = int(curPrice * float(j))
                    for k in message["payload"]["data"]["data_books_cc"][i]["values"].values():
                        cumulative += float(k[min(int(float(j)*10000)-1, len(k)-1)])
                    self.orderbook[i][j] = cumulative
            self.__update_skew()

    def __update_skew(self):
        for i in self.skew.keys():
            self.skew[i] = self.orderbook["bids"][i]/self.orderbook["asks"][i]
        # print(self.skew)
        # print(self.orderbook)
    
    def run(self, precision):
        # d = datetime.datetime.utcnow()
        # epoch = datetime.datetime(1970,1,1)
        # t = (d - epoch).total_seconds()
        # self.orderbook['time'] = int(t)
        if self.ws:
            return self.orderbook, self.skew
        for i in ["bids", "asks"]:
            for j in precision:
                self.orderbook[i][j] = 0
        for i in precision:
            self.skew[i] = 0
        self.connect(self.url)
        set_interval(self._send_heartbeat, 30)
        return {}, {}

def set_interval(func, sec):
    def func_wrapper():
        set_interval(func, sec) 
        func()  
    t = threading.Timer(sec, func_wrapper)
    t.start()
    return t