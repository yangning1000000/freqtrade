from .base_websocket import Websocket
import json
import logging
import asyncio
import urllib3
import time
import datetime
class BinanceFutureWebsocket(Websocket):
    MAX_TABLE_LEN = 200
    def __init__(self):
        super().__init__()
        self.logger.setLevel(logging.ERROR)
        self.url = "wss://fstream.binance.com/ws/btcusd@depth10"
        self.orderbook = {"bids": {}, "asks": {}, "source": "binance_future"}
        self.skew = {}
        self.localBook = {"lastUpdateId": 0}
        self.process_start = False
        self.curPrice = 0
        # self.storedUpdate = []
    
    def _send_command(self):
        self.ws.send(json.dumps({"method": "SUBSCRIBE", "params": ["btcusdt@depth", "btcusdt@ticker"], "id": 1}))

    def _on_message(self, message):
        '''Handler for parsing WS messages.'''
        message = json.loads(message)
        self.logger.debug(json.dumps(message))
        if 'e' not in message:
            return
        if message['e'] == "24hrTicker":
            self.curPrice = float(message['c'])
        if message['e'] == "depthUpdate":
            if message['U'] <= self.localBook["lastUpdateId"] and message["u"] >= self.localBook["lastUpdateId"]:
                self.process_start = True
            elif self.localBook["lastUpdateId"] == 0:
               self.fetchBook()
            if self.process_start:    
                for i in message['b']:
                    self.localBook['bids'][i[0]] = i[1]
                for i in message['a']:
                    self.localBook['asks'][i[0]] = i[1]
                if self.curPrice > 0:
                    self.orderbook["price"] = self.curPrice
                    d = datetime.datetime.utcnow()
                    epoch = datetime.datetime(1970,1,1)
                    t = int((d - epoch).total_seconds())
                    self.orderbook["time"] = t
                    for i in ["bids", "asks"]:
                        for j in self.orderbook[i]:
                            if i == "bids":
                                self.orderbook[i][j] =  sum(map(float, [self.localBook['bids'][i] for i in self.localBook['bids'] if float(i) > self.curPrice * (1 - float(j))]))
                            else:
                                self.orderbook[i][j] =  sum(map(float, [self.localBook['asks'][i] for i in self.localBook['asks'] if float(i) < self.curPrice * (1 + float(j))]))
                    self.__update_skew()

    def __update_skew(self):
        for i in self.skew.keys():
            self.skew[i] = self.orderbook["bids"][i]/self.orderbook["asks"][i]

    def run(self, precision):
        # d = datetime.datetime.utcnow()
        # epoch = datetime.datetime(1970,1,1)
        # t = (d - epoch).total_seconds()
        # self.orderbook['time'] = int(t)
        if self.ws:
            return self.orderbook, self.skew
        for i in ["bids", "asks"]:
            for j in precision:
                self.orderbook[i][j] = 0
        for i in precision:
            self.skew[i] = 0
        self.connect(self.url)
        return {}, {}

    def fetchBook(self):
        http = urllib3.PoolManager()
        r = http.request('GET', 'https://fapi.binance.com/fapi/v1/depth?symbol=BTCUSDT&limit=1000')
        self.localBook = transform_book(json.loads(r.data))
        
def transform_book(book):
    for i in book.keys():
        if isinstance(book[i], list):
            temp = {}
            for j in book[i]:
                temp[j[0]] = j[1]
            book[i] = temp
    return book
        